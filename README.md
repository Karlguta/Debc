Debt case registration application DEBC 

Overview

DEBC is a name I have chosen for this example application to register debt cases.
Technologies used:
•	Java 1.8.0_25
•	SpringBoot 1.5.6
•	Gradle
•	Oracle database 11g Express Edition
•	SoapUI 5.3.0
•	NetBeans 8.2

Debt cases editing protected using authentication – json web token

The following are exposed web services for debt cases editing:
1.	/users/sign-up - Users registration. 
2.	/login – Login into application. Successful login returns json web token as Authorization header.
3.	/customers – Debt case customer’s creation.
4.	/customers/{CustomerPersonalId}/debtcase – Submit debt collection case against the customer.
5.	/customers/{CustomerPersonalId}/debtcase/{1} – Close the debt case as either resolved or defaulted.
Creating customer, submitting and modifying debt cases is protected with json web token (JWT). JWT received during login should be provided in requests Authorization header.

Protection of users seeing each other’s data

Data protection is organized using Oracle database roles. In database each new user receives DEBC_USER role, which has access only to his/her data through database views – CUSTOMER_VIEW, DEBT_CASE_VIEW.

Deployment

Deployment steps:
1.	Execute database scripts
a.	/Scripts/users_roles.sql – execute DEBC_OWN user creation scripts
b.	/Scripts/create_customer.sql – execute CUSTOMER data objects creation scripts with DEBC_OWN user
c.	/Scripts/create_debt_case.sql – execute DEBT_CASE data objects creation scripts with DEBC_OWN user
d.	/Scripts/debc_own_roles.sql – execute DEBC_USER role creation scripts with DEBC_OWN user
2.	cd to folder /Debc/build/libs  containing executable JAR file debc-0.1.0.jar
3.	Execute java –jar debc-0.1.0.jar. If your database connection string differs from Oracle 11g Express Edition default jdbc:oracle:thin:@localhost:1521:XE, then provide it using command line parameter --db.url={your connection string}
4.	Try application using SoapUI project RESTDebc-soapui-project.xml
