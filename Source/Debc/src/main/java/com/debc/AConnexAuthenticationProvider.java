package com.debc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class AConnexAuthenticationProvider implements AuthenticationProvider {

    static final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();

    @Autowired
    Connections connections;

    @Autowired
    private Utils utils;

    private @Value("${db.url}")
    String url;

    @Override
    public Authentication authenticate(Authentication auth)
            throws AuthenticationException {

        //properties for creating connection to Oracle database
        Properties props = new Properties();
        props.setProperty("user", utils.oracleNameFromEmail(auth.getName()));
        props.setProperty("password", auth.getCredentials().toString());

        //creating connection to Oracle database using JDBC
        Connection conn;
        try {
            conn = DriverManager.getConnection(url, props);
            connections.getmConnenctions().put(auth.getName(), conn);

        } catch (SQLException ex) {
            Logger.getLogger(AConnexAuthenticationProvider.class.getName()).log(Level.SEVERE, null, ex);
            throw new AuthenticationException("Bad username or password") {
            };

        }

        return new UsernamePasswordAuthenticationToken(auth.getName(), auth.getCredentials(), AUTHORITIES);
    }

    @Override
    public boolean supports(Class<? extends Object> paramClass) {
        return true;
    }
}
