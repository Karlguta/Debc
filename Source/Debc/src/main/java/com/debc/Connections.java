package com.debc;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class Connections {
    private Map<String,Connection> mConnenctions = new HashMap<>();

    /**
     * @return the mConnenctions
     */
    public Map<String,Connection> getmConnenctions() {
        return mConnenctions;
    }

    /**
     * @param mConnenctions the mConnenctions to set
     */
    public void setmConnenctions(Map<String,Connection> mConnenctions) {
        this.mConnenctions = mConnenctions;
    }
    
    
}
