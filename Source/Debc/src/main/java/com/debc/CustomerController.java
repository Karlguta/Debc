package com.debc;

import static com.debc.SecurityConstants.TOKEN_PREFIX;
import io.jsonwebtoken.Jwts;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")

public class CustomerController {

    @Autowired
    Connections connections;
    
    @Autowired
    Utils utils;

    @PostMapping
    public ResponseEntity addCustomer(@RequestBody Customer customer, @RequestHeader(value = "Authorization") String token) {
        
        // parse the token.
        String user = Jwts.parser()
                .setSigningKey(utils.getSecret())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();
        
        Connection conn = connections.getmConnenctions().get(user);

        PreparedStatement preparedStatement = null;

        String sql = "insert into debc_own.customer_view (name, surname, email, personal_id) values (?,?,?,?)";

        try {
            //creating PreparedStatement object to execute query
            preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getSurname());
            preparedStatement.setString(3, customer.getEmail());
            preparedStatement.setString(4, customer.getPersonalId());

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return new ResponseEntity(customer, HttpStatus.CREATED);

    }
}
