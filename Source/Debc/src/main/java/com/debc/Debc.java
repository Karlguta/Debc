package com.debc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Debc {
    
            
    private @Value("${db.url}") String url;
    private @Value("${db.user}") String user_name;
    private @Value("${db.pwd}") String pwd;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Debc.class, args);
    }

    @Bean
    public Connection connection() throws Exception {


        //properties for creating connection to Oracle database
        Properties props = new Properties();
        props.setProperty("user", user_name);
        props.setProperty("password", pwd);

        //creating connection to Oracle database using JDBC
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, props);

        } catch (SQLException ex) {
            Logger.getLogger(Debc.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("No connection to application user");
        }

        return conn;
    }

}
