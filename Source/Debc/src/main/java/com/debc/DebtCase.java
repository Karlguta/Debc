package com.debc;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class DebtCase {

    private Long debtCaseId;
    private String customerPersonalId;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dueDate;

    private BigDecimal amount;
    private String status; // open, resolved, defaulted

    /**
     * @return the debtCaseId
     */
    public Long getDebtCaseId() {
        return debtCaseId;
    }

    /**
     * @param debtCaseId the debtCaseId to set
     */
    public void setDebtCaseId(Long debtCaseId) {
        this.debtCaseId = debtCaseId;
    }

    /**
     * @return the customerPersonalId
     */
    public String getCustomerPersonalId() {
        return customerPersonalId;
    }

    /**
     * @param customerPersonalId the customerPersonalId to set
     */
    public void setCustomerPersonalId(String customerPersonalId) {
        this.customerPersonalId = customerPersonalId;
    }

    /**
     * @return the dueDate
     */
    public LocalDate getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.debtCaseId + " " + this.customerPersonalId + " " + this.dueDate + " " + this.amount + " " + this.status;
    }

    public void setDebtCaseIdNext(Connection conn) throws SQLException {
        String sql = "select debc_own.debt_case_seq.nextval as next_id from dual";

        try (PreparedStatement preStatement = conn.prepareStatement(sql)) {

//creating PreparedStatement object to execute query
            ResultSet result = preStatement.executeQuery();

            while (result.next()) {
                debtCaseId = result.getLong("next_id");
            }

        }

    }

}
