package com.debc;

import static com.debc.SecurityConstants.TOKEN_PREFIX;
import io.jsonwebtoken.Jwts;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DebtCaseController {

    @Autowired
    Connections connections;

    @Autowired
    Utils utils;

    @PostMapping("/customers/{personalId}/debtcase")
    public ResponseEntity addDebtCase(
            @PathVariable("personalId") String personalId,
            @RequestBody DebtCase debtCase,
            @RequestHeader(value = "Authorization") String token) {

        debtCase.setCustomerPersonalId(personalId);
        
        // parse the token.
        String user = Jwts.parser()
                .setSigningKey(utils.getSecret())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();
        
        Connection conn = connections.getmConnenctions().get(user);

        PreparedStatement preparedStatement = null;

        String sql = "insert into debc_own.debt_case_view (debt_case_id, due_date, amount, cust_personal_id) values (?,?,?,?)";

        try {
            debtCase.setDebtCaseIdNext(conn);

            //creating PreparedStatement object to execute query
            preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setLong(1, debtCase.getDebtCaseId());
            preparedStatement.setDate(2, java.sql.Date.valueOf(debtCase.getDueDate()));
            preparedStatement.setBigDecimal(3, debtCase.getAmount());
            preparedStatement.setString(4, debtCase.getCustomerPersonalId());

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DebtCaseController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DebtCaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return new ResponseEntity(debtCase, HttpStatus.CREATED);

    }

    @PatchMapping("/customers/{personalId}/debtcase/{id}")
    public ResponseEntity updDebtCaseStatus(
            @PathVariable("personalId") String personalId,
            @PathVariable("id") Long debtCaseId,
            @RequestBody DebtCase debtCase,
            @RequestHeader(value = "Authorization") String token) {

        debtCase.setCustomerPersonalId(personalId);
        debtCase.setDebtCaseId(debtCaseId);
        // parse the token.
        String user = Jwts.parser()
                .setSigningKey(utils.getSecret())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();
        
        Connection conn = connections.getmConnenctions().get(user);

        PreparedStatement preparedStatement = null;

        String sql = "update debc_own.debt_case_view set status = ? where debt_case_id = ?";

        try {

            //creating PreparedStatement object to execute query
            preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, debtCase.getStatus());
            preparedStatement.setLong(2, debtCase.getDebtCaseId());

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DebtCaseController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DebtCaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return new ResponseEntity(debtCase, HttpStatus.OK);

    }

}
