package com.debc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    private Connection connection;

    public UserController(Connection connection) {
        this.connection = connection;
    }
    
    @Autowired
    Utils utils;
    

    @PostMapping("/sign-up")
    public ResponseEntity signUp(@RequestBody ApplicationUser user) {

        // create user
        // assign role debc_user
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute("create user " + utils.oracleNameFromEmail(user.getEmail()) + " identified by " + user.getPassword());
            statement.execute("grant debc_user to " + utils.oracleNameFromEmail(user.getEmail()));
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if(statement!=null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return new ResponseEntity(HttpStatus.CREATED);
    }

}
