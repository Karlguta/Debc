package com.debc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Utils {
    
    private @Value("${jwt.secret}") String secret;
    private @Value("${jwt.expirationtime}") long expirationTime;
    
    
    public String oracleNameFromEmail(String email) {
        return email.replace('@', '_').replace('.','_');
    }

    /**
     * @return the secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * @return the expirationTime
     */
    public long getExpirationTime() {
        return expirationTime;
    }
    
    
    
}
