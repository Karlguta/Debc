package com.debc;

import static com.debc.SecurityConstants.HEADER_STRING;
import static com.debc.SecurityConstants.LOGIN_URL;
import static com.debc.UserTest.USER_NAME;
import static com.debc.UserTest.USER_PWD;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerTest {

    @Value("${local.server.port}")
    int port;

    String localHostUrl;

    String jwtToken;

    public CustomerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        localHostUrl = "http://localhost:" + port;
        // Get jwt Authorization token

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder);

        String url = localHostUrl + LOGIN_URL;
        ApplicationUser user = new ApplicationUser();
        user.setEmail(USER_NAME);
        user.setPassword(USER_PWD);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, user, String.class);

        HttpHeaders httpHeaders = responseEntity.getHeaders();
        jwtToken = httpHeaders.getFirst(HEADER_STRING);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void createCustomer() {

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder);

        String url = localHostUrl + "/customers";
        Customer customer = new Customer();
        customer.setName("Name");
        customer.setSurname("Surname");
        customer.setEmail("Email");
        customer.setPersonalId("PersonalId");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HEADER_STRING, jwtToken);

        HttpEntity<Customer> requestEntity = new HttpEntity<>(customer, httpHeaders);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);

        assertTrue(HttpStatus.CREATED.equals(responseEntity.getStatusCode()));

    }
}
