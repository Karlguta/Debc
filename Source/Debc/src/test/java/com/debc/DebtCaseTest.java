package com.debc;

import static com.debc.SecurityConstants.HEADER_STRING;
import static com.debc.SecurityConstants.LOGIN_URL;
import static com.debc.UserTest.USER_NAME;
import static com.debc.UserTest.USER_PWD;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DebtCaseTest {

    @Value("${local.server.port}")
    int port;

    String localHostUrl;

    String jwtToken;

    public DebtCaseTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        localHostUrl = "http://localhost:" + port;
        // Get jwt Authorization token

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder);

        String url = localHostUrl + LOGIN_URL;
        ApplicationUser user = new ApplicationUser();
        user.setEmail(USER_NAME);
        user.setPassword(USER_PWD);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, user, String.class);

        HttpHeaders httpHeaders = responseEntity.getHeaders();
        jwtToken = httpHeaders.getFirst(HEADER_STRING);

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void createDebtCase() {

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder);

        String url = localHostUrl + "/customers/PersonalId/debtcase";

        DebtCase debtCase = new DebtCase();
        debtCase.setAmount(new BigDecimal("123.12"));
        debtCase.setDueDate(LocalDate.now());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HEADER_STRING, jwtToken);

        HttpEntity<DebtCase> requestEntity = new HttpEntity<>(debtCase, httpHeaders);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);

        assertTrue(HttpStatus.CREATED.equals(responseEntity.getStatusCode()));

    }

    @Test
    public void patchDebtCase() {

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder.requestFactory(requestFactory));

        String url = localHostUrl + "/customers/PersonalId/debtcase";

        DebtCase debtCase = new DebtCase();
        debtCase.setAmount(new BigDecimal("123.12"));
        debtCase.setDueDate(LocalDate.now());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HEADER_STRING, jwtToken);

        HttpEntity<DebtCase> requestEntity = new HttpEntity<>(debtCase, httpHeaders);

        ResponseEntity<DebtCase> responseEntity = restTemplate.postForEntity(url, requestEntity, DebtCase.class);

        DebtCase dcStatus = responseEntity.getBody();

        url = url + "/" + dcStatus.getDebtCaseId();
        dcStatus.setStatus("Resolved");

        HttpEntity<DebtCase> requestPatchEntity = new HttpEntity<>(dcStatus, httpHeaders);

        DebtCase result = restTemplate.patchForObject(url, requestPatchEntity, DebtCase.class);

        assertTrue(dcStatus.getDebtCaseId().equals(result.getDebtCaseId()));

    }

}
