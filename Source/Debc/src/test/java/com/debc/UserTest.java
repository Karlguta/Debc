package com.debc;

import static com.debc.SecurityConstants.SIGN_UP_URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class UserTest {
    
    
    public static final String USER_NAME = "debc.test@test.com";
    public static final String USER_NAME_SIGN_UP = "debc.signup@test.com";
    public static final String USER_PWD = "debc_test";
    
    
    public UserTest() {
    }
    
    @Value("${local.server.port}") 
    int port;
    
    String localHostUrl;
    
    @Autowired
    private Connection connection;
    
    @Autowired
    Utils utils;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        localHostUrl = "http://localhost:" + port;
        
        // drop user
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute("drop user " + utils.oracleNameFromEmail(USER_NAME_SIGN_UP));
        } catch (SQLException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement!=null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    

    
    
    @Test
    public void signUp() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        TestRestTemplate  restTemplate = new TestRestTemplate(restTemplateBuilder);
        
        String url = localHostUrl + SIGN_UP_URL;
        ApplicationUser user = new ApplicationUser();
        user.setEmail(USER_NAME_SIGN_UP);
        user.setPassword(USER_PWD);
        
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, user, String.class);
        
        assertTrue(HttpStatus.CREATED.equals(responseEntity.getStatusCode()));
        
    }
}
