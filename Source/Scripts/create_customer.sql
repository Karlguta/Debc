CREATE TABLE CUSTOMER 
(
  CUSTOMER_ID NUMBER(10) NOT NULL 
, USER_NAME VARCHAR2(30) NOT NULL 
, NAME VARCHAR2(20) 
, SURNAME VARCHAR2(20) 
, EMAIL VARCHAR2(50) 
, PERSONAL_ID VARCHAR2(20) 
, CONSTRAINT CUSTOMER_PK PRIMARY KEY 
  (
    CUSTOMER_ID 
  )
  ENABLE 
);

CREATE SEQUENCE CUSTOMER_SEQ;

CREATE OR REPLACE TRIGGER CUSTOMER_INS 
BEFORE INSERT ON CUSTOMER 
   FOR EACH ROW

DECLARE
   v_username varchar2(30);

BEGIN
   -- Find username of person performing INSERT into table
   SELECT user INTO v_username
   FROM dual;

   -- Update created_by field to the username of the person performing the INSERT
   :new.user_name := v_username;
   
   SELECT CUSTOMER_SEQ.NEXTVAL 
    INTO :new.customer_id
    FROM dual;
   
END;

CREATE OR REPLACE TRIGGER CUSTOMER_UPD 
BEFORE UPDATE OF USER_NAME ON CUSTOMER 
FOR EACH ROW 
BEGIN
  raise PROGRAM_ERROR;
END;

CREATE VIEW CUSTOMER_VIEW
AS SELECT 
    *
FROM CUSTOMER a
WHERE a.user_name = user;