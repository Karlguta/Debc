CREATE TABLE DEBT_CASE 
(
  DEBT_CASE_ID NUMBER(10) NOT NULL 
, USER_NAME VARCHAR2(30) NOT NULL 
, CUST_PERSONAL_ID VARCHAR2(20) 
, DUE_DATE DATE 
, AMOUNT NUMBER 
, STATUS VARCHAR2(20) 
, CONSTRAINT DEBT_CASE_PK PRIMARY KEY 
  (
    DEBT_CASE_ID 
  )
  ENABLE 
);

CREATE SEQUENCE DEBT_CASE_SEQ;

CREATE OR REPLACE TRIGGER DEBT_CASE_INS 
BEFORE INSERT ON DEBT_CASE 
   FOR EACH ROW

DECLARE
   v_username varchar2(30);

BEGIN
   -- Find username of person performing INSERT into table
   SELECT user INTO v_username
   FROM dual;

   -- Update created_by field to the username of the person performing the INSERT
   :new.user_name := v_username;
   
END;

CREATE OR REPLACE TRIGGER DEBT_CASE_UPD 
BEFORE UPDATE OF USER_NAME ON DEBT_CASE 
FOR EACH ROW 
BEGIN
  raise PROGRAM_ERROR;
END;

CREATE VIEW DEBT_CASE_VIEW
AS SELECT 
    *
FROM DEBT_CASE a
WHERE a.user_name = user;
