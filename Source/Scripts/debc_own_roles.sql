create role debc_user;

grant create session to debc_user;
grant select, insert, update, delete on customer_view to debc_user;
grant select, insert, update, delete on debt_case_view to debc_user;
grant select on debt_case_seq to debc_user;

create user debc_test_test_com identified by "debc_test";
grant debc_user to debc_test_test_com;