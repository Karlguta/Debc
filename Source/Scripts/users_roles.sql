create user debc_own identified by "debc_own";

grant connect to debc_own;
grant resource to debc_own;
grant create view to debc_own;

grant create user to debc_own;
grant drop user to debc_own;
grant create role to debc_own;
grant grant any role to debc_own;
grant grant any privilege to debc_own;
